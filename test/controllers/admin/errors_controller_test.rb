require 'test_helper'

class Admin::ErrorsControllerTest < ActionController::TestCase
  setup do
    @admin_error = admin_errors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_errors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_error" do
    assert_difference('Admin::Error.count') do
      post :create, admin_error: { app_name: @admin_error.app_name, class_name: @admin_error.class_name, doc_root: @admin_error.doc_root, id: @admin_error.id, params: @admin_error.params, referer_url: @admin_error.referer_url, target_url: @admin_error.target_url, trace: @admin_error.trace, user_agent: @admin_error.user_agent, user_info: @admin_error.user_info }
    end

    assert_redirected_to admin_error_path(assigns(:admin_error))
  end

  test "should show admin_error" do
    get :show, id: @admin_error
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_error
    assert_response :success
  end

  test "should update admin_error" do
    patch :update, id: @admin_error, admin_error: { app_name: @admin_error.app_name, class_name: @admin_error.class_name, doc_root: @admin_error.doc_root, id: @admin_error.id, params: @admin_error.params, referer_url: @admin_error.referer_url, target_url: @admin_error.target_url, trace: @admin_error.trace, user_agent: @admin_error.user_agent, user_info: @admin_error.user_info }
    assert_redirected_to admin_error_path(assigns(:admin_error))
  end

  test "should destroy admin_error" do
    assert_difference('Admin::Error.count', -1) do
      delete :destroy, id: @admin_error
    end

    assert_redirected_to admin_errors_path
  end
end
