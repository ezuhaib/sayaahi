module ApplicationHelper
  def pretty_created_at(object)
    object.created_at.strftime("#{object.created_at.day.ordinalize} %b %Y <i>at</i> <b>%H:%m</b>").html_safe
  end

  def pretty_updated_at(object)
    object.updated_at.strftime("#{object.updated_at.day.ordinalize} %b %Y <i>at</i> <b>%H:%m</b>").html_safe
  end

  # If either value is nil, returns the non-nil value.
  # If both values non-nil , then performs diff
  def xdiff(before,after)
    if !before && !after
      return false
    elsif !before
      return after
    elsif !after
      return before
    else
      DiffBuilder.new(before, after).build.html_safe
    end
  end

  def time_ago time, append = 'ago'
    return time_ago_in_words(time).gsub(/about|less than|almost|over/, '').strip.capitalize << " " + append
  end

  def time_left_badge time, append = 'left'
    if time > Time.now
      return content_tag :span , class: 'badge badge-success' do
        time_ago_in_words(time).gsub(/about|less than|almost|over/, '').strip.capitalize << " " + append
      end
    else
      return content_tag :span , class: 'badge bg-light' do
        "Expired"
      end
    end
  end

  def notifications_count
    count = PublicActivity::Activity.where(:recipient_id => current_user.id,read: false).count
  end

  # Ensure that the comments passed into this method
  # have made their way through the ancestry gem by use
  # of .arrange command
  def nested_comments(comments)
    comments.map do |comment, sub_comments|
      render(comment) + content_tag(:div, nested_comments(sub_comments), :class => "nested_comments")
    end.join.html_safe
  end

  def badge_for(category)
    content_tag :div , class: "category-badge" , style:"background-color: #{category.color_light}" do
      fa_icon category.icon , text: category.name
    end
  end

  def mini_badge_for(category)
    content_tag :div , class: "category-mini-badge" , style:"background-color: #{category.color_dark}" , title: category.name, "data-toggle" => "tooltip" do
      fa_icon category.icon
    end
  end

  def styled_tag(key)
    content_tag :div , class: "tag tag-#{key}" do
      key
    end
  end

  def workflow_form_controls(object)
    r = ""
    object.current_state.events.each do |e|
      e = e[1][0] # See https://github.com/geekq/workflow/issues/151 for explanation of this
      friendly_name = e.meta[:friendly] || e.name.capitalize
      unless e.meta[:hide] == true
        r << button_tag(friendly_name , value: e.name , class: "workflow-btn workflow-btn-#{e.name}")
        r << '&nbsp'
      end
    end
    return r.html_safe
  end

  def link_to_user (user_id)
    @user = User.find(user_id)
    link_to @user.username, @user if @user
  end
end
