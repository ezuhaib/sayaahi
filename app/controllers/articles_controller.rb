class ArticlesController < ApplicationController

  before_action :set_article, only: [:show, :workroom, :workroom_save, :edit, :update, :destroy,
    :crop, :version, :destroy_version, :assign, :assign_save]

  # GET /articles
  # GET /articles.json
  def index
    authorize! :read , Article
    @feature = Article.where(workflow_state: 'featured').first
    if params[:category] and Category.where(slug:params[:category]).present?
      @cat = Category.where(slug:params[:category]).first
      ahoy.track "navigation_page" , {} , trackable: @cat
      @page_title  = @cat.name
      @page_description = @cat.description
      set_tab @cat.slug
      @articles = @cat.articles.published.order('created_at DESC').page(params[:page]).per(5)
      render 'category_articles'
    elsif params[:category] # if a category is specified but doesn't exist
      raise ActionController::RoutingError.new("No route or channel for: #{params[:category]}")
    end
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    authorize! :read , @article
    @page_title  = @article.title
    @page_description = @page_description  = teaser(@article.body)
    @more = @cat.articles.published.first(5)
    @og_image = @article.cover(:large)
    ahoy.track "readable_content" , {} , trackable: @article unless @article.user == current_user
    authorize! :read_unpublished , @article unless @article.published?
    @comment = Comment.where(commentable: @article).new
    @comments = @article.comments.where(workflow_state: 'accepted').arrange
  end

  def workroom
    authorize! :work , @article
    @page_title = "WorkRoom for #{@article.title}"
    @page_description = "Editorial tasks section for the user-submitted nodes."
    @activities = @article.activities.where.not(key: 'article.commented').order('created_at DESC')
    @assignments = @article.assignments
    @hide_sidebar = true
  end

  def workroom_save
    #todo: validate this input
    #todo: more granular permissions MUST be here
    #todo: the user may have to be notified about some of the actions here
    authorize! :work , @article
    @article.create_activity params[:button], owner: current_user, parameters:{remarks: params[:remarks]}
    if @article.current_state.events.include? params[:button].to_sym
      @article.send(params[:button]+"!")
    end
    redirect_to :back
  end

  def version
    authorize! :read_versions , @article
    @page_title  = "Previous versions of #{@article.title}"
    @ver = params[:version]
    if @ver
      if @ver == "original"
        @editor = @article.user
        @article = @article.versions.first.reify
        @article_prev = nil
      else
        @editor = User.find(@article.previous_version.originator)
        @article = @article.versions.find(@ver).next.try(:reify) || @article
      end
    end
  end

  def assign
    authorize! :assign , @article
    @page_title  = "Assigning node"
    @teammates = Role.where(title: 'teammate').first.try(:users)
    @assignments = @article.assignments
    @assignment = Assignment.new(assignable: @article)
    @hide_sidebar = true
    if @teammates.blank?
      flash[:error] =  "No Sayaahi Teammates defined so far"
      redirect_to :back
    end
  end

  def assign_save
    authorize! :assign , @article
    @page_title  = "Assigning node"
    @assignments = @article.assignments
    @assignment = Assignment.new(assignment_params)
    @assignment.by_id = current_user.id
    @teammates = Role.where(title: 'teammate').first.try(:users)

    if @assignment.save
      @article.assign! if @article.awaiting_assignment?
      @article.create_activity :assign , owner: @assignment
      flash[:success] = "Task assignment successful"
      @article.assign! if @article.awaiting_assignment?
      redirect_to workroom_article_path
    else
      flash[:error] = "There was an error"
      render :assign
    end
  end

  # GET /articles/new
  def new
    authorize! :create , Article
    @page_title  = "New article submission"
    @article = Article.new
    if params[:cat]
      @cat = Category.find(params[:cat])
      set_tab @cat.name
    end
  end

  # GET /articles/1/edit
  def edit
    authorize! :update , @article
    @page_title  = "Editing #{@article.title}"
    if params[:version]
      @ver = params[:version]
      @article = @article.versions.find(@ver).next.try(:reify) || @article
    end
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)
    @article.user_id = current_user.id
    authorize! :create , @article
    if params[:article][:cover] && @article.save
      redirect_to crop_cover_path (@article)
    elsif @article.save
      @article.create_activity :create , owner: current_user
      redirect_to @article, notice: "Article submitted for approval. You'll be notified upon aproval"
    else
      render :new
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    authorize! :update , @article
    @article.assign_attributes(article_params)
    @category_change = @article.category_id_change
    @article_body_or_title_changed = (@article.body_changed? or @article.title_changed?)
    @article.save

    # By now the node is already saved.

    if @article.errors.any?
      flash[:error] = @article.errors.full_messages_for(:body).first
      render :edit
    elsif params[:article][:cover] && @article.save
      redirect_to action: :crop
    else
      @article.create_activity :update , owner: current_user ,
                                parameters: {version: @article.versions.last.id, remarks: params[:article][:remarks]} if @article_body_or_title_changed

      @article.create_activity  :category_changed , owner: current_user , recipient: @article.user,
                                parameters: {from: @category_change[0] , to: @category_change[1]} if @category_change

      redirect_to @article, notice: 'Article was successfully updated.'
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    authorize! :destroy , @article
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def crop
    authorize! :update , @article
    @page_title  = "Article image cropping"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
      @cat = @article.category
      set_tab @cat.name
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :body, :cover, :category_id, :created_at , :intro, cropping_params(:cover))
    end

    def assignment_params
      params.require(:assignment).permit(:by_id, :to_id, :assignable_id, :assignable_type, :tag, :deadline, :body, :title)
    end
end
