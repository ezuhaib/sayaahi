class AssignmentsController < ApplicationController
  before_action :set_assignment, only: [:show, :edit, :update, :destroy, :workflow]
  before_action :set_theming
  respond_to :html
  set_tab :assignments

  def index
    authorize! :read , Assignment
    @page_title = "Assignments"
    @active_assignments = current_user.assignments.active
    if params[:q] and params[:q] == 'expired'
      @assignments = current_user.assignments.expired.order('created_at DESC')
    elsif params[:q] and params[:q] == 'all'
      @assignments = current_user.assignments.order('created_at DESC')
    elsif params[:q] and params[:q] == 'refused'
      @assignments = current_user.assignments.where(workflow_state: 'refused').order('created_at DESC')
    else
      @assignments = @active_assignments.order('created_at DESC')
    end

    set_tab params[:q] || 'active'
    respond_with(@assignments)
  end

  def show
    authorize! :read , @assignment
    @page_title = "Showing Assignment"
    flashed_comments = {commentable_type: 'Assignment' , commentable_id: @assignment.id}.merge(flash[:comment] ||= {})
    @comment = Comment.new(flashed_comments)
    @comments = @assignment.comments.arrange
    respond_with(@assignment)
  end

  def new
    authorize! :create , Assignment
    @assignment = Assignment.new
    @teammates = Role.where(title: 'teammate').first.try(:users)
    if @teammates.blank?
      flash[:error] = "Either teammate role not defined, or no users added to that role."
      redirect_to :root
    end
  end

  def edit
    authorize! :update , Assignment
  end

  def create
    authorize! :create , Assignment
    @assignment = Assignment.new(assignment_params)
    @assignment.by_id = current_user.id
    @teammates = Role.where(title: 'teammate').first.try(:users)
    if @assignment.save
      redirect_to new_assignment_path, notice: "Assigned successfully"
    else
      render :new
    end
  end

  def update
    authorize! :create , Assignment
    @assignment.update(assignment_params)
    respond_with(@assignment)
  end

  def destroy
    authorize! :destroy , @assignment
    @assignment.destroy
    respond_with(@assignment)
  end

  def workflow
    if params[:do] and @assignment.open?
      if params[:do] == 'excuse'
        @assignment.refuse!
        flash[:notice] = "The assignment was refused."
      elsif params[:do] == 'complete'
        @assignment.complete!
        flash["success"] = "Assignment marked as complete."
      else
        flash[:error] = "Workflow update failed"
      end
    else
      flash[:error] = "Workflow update failed"
    end
    redirect_to :back
  end

  private
    def set_assignment
      @assignment = Assignment.find(params[:id])
    end

    def assignment_params
      params.require(:assignment).permit(:by_id, :to_id, :assignable_id, :assignable_type, :tag, :deadline, :body, :title)
    end

    def set_theming
      @cat = Category.new(color_dark: '#244d8a',color_light: '#2c63b6')
    end
end
