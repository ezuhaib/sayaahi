class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy, :accept, :reject]

  # GET /comments
  # GET /comments.json
  def index
    authorize! :read , Comment
    @comments ||= Comment.all
  end

  def accept
    if @comment.accept!
      flash[:success] = "Anonymous comment accepted"
      redirect_to admin_comments_path
    end
  end

  def reject
    if @comment.reject!
      flash[:error] = "Anonymous comment rejected"
      redirect_to admin_comments_path
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    authorize! :read , @comment
    @comment.user ||= User.new
  end

  # GET /comments/new
  def new
    authorize! :create , Comment
    @comment = Comment.new(parent_id: params[:parent_comment])
    if @parent = Comment.find(params[:parent_comment])
      @comment.commentable_type = @parent.commentable_type
      @comment.commentable_id = @parent.commentable_id
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /comments/1/edit
  def edit
    authorize! :update , @comment
  end

  # POST /comments
  # POST /comments.json
  def create
    authorize! :create , Comment
    @comment = Comment.new(comment_params)
    @comment.user_id = current_user.try(:id)
    if @comment.save
      redirect_to @comment.commentable, notice: 'Comment was successfully created.'
    else
      flash[:error] = @comment.errors.full_messages.join(', ')
      flash[:comment] = @comment
      redirect_to :back
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    authorize! :update , @comment
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    authorize! :destroy , @comment
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url, notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:body, :commentable_type, :commentable_id, :parent_id, :name, :email)
    end
end
