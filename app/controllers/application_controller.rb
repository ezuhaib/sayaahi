class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :record_signup_redirect_path
  before_action :configure_devise_permitted_parameters, if: :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    if current_user
      flash[:error] = "You are not authorized to access this page"
      redirect_to root_path
    else
      flash[:error] = "Must login first"
      redirect_to user_session_path
    end
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  include ActionView::Helpers::SanitizeHelper
  def teaser(text)
    strip_tags(text.truncate(160))
  end

  protected

  # For unauthenticated users, reference to each page request
  # is saved in the session so that upon sign-in user is
  # correctly referred to the last page he was on.

  def record_signup_redirect_path
    return if current_user and !request.get?
    if (request.path != "/account" && # This page is rendered if there was error upon sign up
        request.path != "/account/login" &&
        request.path != "/account/register" &&
        request.path != "/account/password/new" &&
        request.path != "/account/logout" &&
        !request.xhr?) # don't store ajax calls
      session[:user_return_to] = request.fullpath
    end
  end

  def configure_devise_permitted_parameters
    registration_params = [:username, :login, :dob, :gender, :body, :email, :password, :password_confirmation, User.option_params]

    if params[:action] == 'update'
      devise_parameter_sanitizer.for(:account_update) {
        |u| u.permit(registration_params << :current_password)
      }

    elsif params[:action] == 'create'
      devise_parameter_sanitizer.for(:sign_up) {
        |u| u.permit(registration_params << :username)
      }
    end
  end
end
