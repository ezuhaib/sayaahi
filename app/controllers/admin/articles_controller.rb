class Admin::ArticlesController < ApplicationController

set_tab :articles

def index
  authorize! :moderate , Article
  if params[:show] == "all"
    @articles = Article.all
  elsif params[:show] == "rejected"
    @articles = Article.where(workflow_state: 'rejected')
  elsif params[:show] == "assigned"
    @articles = Article.where(workflow_state: 'assigned')
  else
    @articles = Article.where(workflow_state: ['awaiting_assignment'])
  end
end

end
