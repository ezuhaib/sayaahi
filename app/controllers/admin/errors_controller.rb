class Admin::ErrorsController < ApplicationController
  before_action :set_admin_error, only: [:show, :edit, :update, :destroy]
  before_filter :check_permissions
  respond_to :html

  def index
    @admin_errors = Admin::Error.order('created_at DESC').all
    respond_with(@admin_errors)
  end

  def show
    respond_with(@admin_error)
  end

  def destroy
    @admin_error.destroy
    respond_with(@admin_error)
  end

  private
    def set_admin_error
      @admin_error = Admin::Error.find(params[:id])
    end

    def admin_error_params
      params.require(:admin_error).permit(:id, :class_name, :trace, :params, :target_url, :referer_url, :user_agent, :user_info, :app_name, :doc_root)
    end

    def check_permissions
      authorize! :manage , :errors
    end
end
