class Admin::CommentsController < ApplicationController

def index
  authorize! :moderate , Comment
  if params[:show] == "all"
    @comments = Comment.all
  elsif params[:show] == "rejected"
    @comments = Comment.where(workflow_state: 'rejected')
  else
    @comments = Comment.where(workflow_state: 'awaiting_review')
  end
end

def show
  @comment = Comment.find(params[:id])
end

end
