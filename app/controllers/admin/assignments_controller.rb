class Admin::AssignmentsController < ApplicationController

def index
	@assignments = Assignment.all.page(params[:page]).order('created_at DESC')
end

end