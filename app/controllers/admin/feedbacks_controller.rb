class Admin::FeedbacksController < ApplicationController

def index
  authorize! :read_hidden , Feedback
  @feedbacks = Feedback.where(hidden: 1).order("created_at DESC").page(params[:page])
end

end
