class Admin::PagesController < ApplicationController

def index
authorize! :access , :admin
@page_title  = "Admin"
@articles_count = Article.where(workflow_state: "awaiting_review").count
@comments_count = Comment.where(workflow_state: "awaiting_review").count
end

def assignments
  authorize! :access , :admin
  @assignments = Assignment.order('created_at DESC').page(params[:page])
end

end
