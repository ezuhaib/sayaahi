class Admin::TaskTypesController < ApplicationController
  before_action :set_task_type, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @task_types = TaskType.all
    respond_with(@task_types)
  end

  def show
    respond_with(@task_type)
  end

  def new
    @task_type = TaskType.new
    respond_with(@task_type)
  end

  def edit
  end

  def create
    @task_type = TaskType.new(task_type_params)
    if @task_type.save
      redirect_to admin_task_types_path, notice: "New task type added successfuly"
    else
      render :new
    end
  end

  def update
    @task_type.update(task_type_params)
    redirect_to admin_task_types_path , notice: 'Task type updates successfully'
  end

  def destroy
    @task_type.destroy
    redirect_to admin_task_types_path, notice: "New task type REMOVED successfuly"
  end

  private
    def set_task_type
      @task_type = TaskType.find(params[:id])
    end

    def task_type_params
      params.require(:task_type).permit(:key, :title, :body, :assignable_type, :assignee_limit, :guide_page_slug)
    end
end
