class Admin::CategoriesController < ApplicationController

  before_action :set_category, only: [:show, :edit, :update, :destroy, :remove]

  # GET /admin/categories
  # GET /admin/categories.json
  def index
  	authorize! :read , Category
  	@categories = Category.all
  end

  # GET /admin/categories/1
  # GET /admin/categories/1.json
  def show
    authorize! :read , @category
    @page_title  = @category.name
    @page_description = teaser(@category.description) if @category.description
  end

  # GET /admin/categories/new
  def new
    authorize! :create , Category
    @page_title  = "New Category"
    @category = Category.new
  end

  # GET /admin/categories/1/edit
  def edit
    authorize! :update , @category
    @page_title  = "Editing #{@category.name}"
  end

  # POST /admin/categories
  # POST /admin/categories.json
  def create
    authorize! :create , Category
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to [:admin, @category], notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/categories/1
  # PATCH/PUT /admin/categories/1.json
  def update
    authorize! :update , @category
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to admin_category_path(@category), notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  def remove
  end

  # DELETE /admin/categories/1
  # DELETE /admin/categories/1.json
  def destroy
    authorize! :delete , @category
    if @category.articles.count == 0
      @category.destroy
      redirect_to admin_categories_url, notice: 'Category was successfully destroyed.'
    else
      flash[:error] = "The catgeory has associated articles and cannot be destroyed."
      redirect_to :back
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name, :description, :intro, :icon, :color_dark, :color_light, :slug, :weight)
    end

end
