class UpdatesController < ApplicationController
  before_action :set_update, only: [:show, :edit, :update, :destroy, :crop]

  respond_to :html

  def index
    authorize! :read , Update
    @page_title  = "News & Updates"
    @page_description = "Updates related to Sayaahi.com itself."
    @updates = Update.order('created_at DESC').page(params[:page]).per(10)
  end

  def show
    @page_title  = @update.title
    @page_description  = teaser(@update.body)
    ahoy.track "readable_content" , {} , trackable: @update unless @update.user == current_user
    respond_with(@update)
  end

  def new
    authorize! :create , Update
    @page_title  = "Creating update"
    @update = Update.new
  end

  def edit
  end

  def create
    authorize! :create , Update
    @update = Update.new(update_params)
    @update.user_id = current_user.id
    if params[:update][:promo] and @update.save
      redirect_to crop_promo_path(@update)
    elsif @update.save
      redirect_to @update
    else
      render :new
    end
  end

  def update
    authorize! :update , Update
    @update.assign_attributes(update_params)
    if params[:update][:promo] and @update.save
      redirect_to crop_promo_path(@update)
    elsif @update.save
      redirect_to @update
    else
      render :update
    end
  end

  def crop
    authorize! :update , @update
    @page_title  = "Article image cropping"
  end

  def destroy
    authorize :destroy , @update
    @update.destroy
    respond_with(@update)
  end

  private
    def set_update
      @update = Update.find(params[:id])
    end

    def update_params
      params.require(:update).permit(:title, :body, :user_id, :active, :promo, cropping_params(:promo))
    end
end
