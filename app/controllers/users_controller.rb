class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :crop]

  # GET /users
  # GET /users.json
  def index
    authorize! :manage , User
    @users = User.all
    @page_title  = "List of all users"
  end

  # GET /users/1
  # GET /users/1.json
  def show
    authorize! :read , @user
    @articles = @user.articles.published.all
    @page_title  = "#{@user.username}'s Profile"
    @page_description = @user.intro
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    authorize! :update , @user
  end

  def crop
    authorize! :update , @user
    @page_title  = "Avatar crop"
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.user_id = current_user.id
    authorize! :create , @user

    if params[:user][:featured_image] && @user.submit!
      redirect_to crop_featured_image_path (@user)
    elsif @user.submit!
      redirect_to @user, notice: "User created"
    else
      render :new
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    authorize! :update , @user
    @page_title  = "Editing Profile"
    if params[:user][:avatar] && @user.update(user_params)
      redirect_to action: :crop
    elsif @user.update(user_params)
      redirect_to @user, notice: 'Profile was successfully updated.'
    else
      render :edit
    end
  end

  def password_changed
    flash[:success] = "Password successfully changed."
    redirect_to root_path
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def autocomplete
    authorize! :read , User
    @users = User.select([:id,:username]).where("username LIKE ?", "%#{params[:query].downcase}%")
    render json: @users
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :fullname, :dob, :gender, :body, :intro, :avatar, cropping_params(:avatar))
    end
end
