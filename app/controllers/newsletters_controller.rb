class NewslettersController < ApplicationController
  before_action :set_newsletter, only: [:show, :edit, :update, :destroy, :mail, :preview]
  before_action :authorize

  def mail
    if params[:test] == 'true'
      NewsletterMailer.main(@newsletter,User.first).deliver
    else
      User.find_each do |user|
        NewsletterMailer.main(@newsletter,user).deliver
      end
    end
    flash[:notice] = "Mail placed in queue"
    redirect_to @newsletter
  end

  def preview
    @body = Liquid::Template.parse(@newsletter.body)
    render layout: false
  end

  # GET /newsletters
  # GET /newsletters.json
  def index
    @newsletters = Newsletter.all
    @page_title  = "Newsletters list"
  end

  # GET /newsletters/1
  # GET /newsletters/1.json
  def show
    @body = Liquid::Template.parse(@newsletter.body)
    @page_title  = @newsletter.title
  end

  # GET /newsletters/new
  def new
    @newsletter = Newsletter.new
    @page_title  = "Creating new newsletter"
  end

  # GET /newsletters/1/edit
  def edit
    @page_title  = "Editing newsletter"
  end

  # POST /newsletters
  # POST /newsletters.json
  def create
    @newsletter = Newsletter.new(newsletter_params)

    respond_to do |format|
      if @newsletter.save
        format.html { redirect_to @newsletter, notice: 'Newsletter was successfully created.' }
        format.json { render :show, status: :created, location: @newsletter }
      else
        format.html { render :new }
        format.json { render json: @newsletter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /newsletters/1
  # PATCH/PUT /newsletters/1.json
  def update
    respond_to do |format|
      if @newsletter.update(newsletter_params)
        format.html { redirect_to @newsletter, notice: 'Newsletter was successfully updated.' }
        format.json { render :show, status: :ok, location: @newsletter }
      else
        format.html { render :edit }
        format.json { render json: @newsletter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /newsletters/1
  # DELETE /newsletters/1.json
  def destroy
    @newsletter.destroy
    respond_to do |format|
      format.html { redirect_to newsletters_url, notice: 'Newsletter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_newsletter
      @newsletter = Newsletter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def newsletter_params
      params.require(:newsletter).permit(:title, :body, :category)
    end

    def authorize
      authorize! :mail , User
    end
end
