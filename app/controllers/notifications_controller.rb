class NotificationsController < ApplicationController
  def index
    @page_title  = "Notifications"
    @activities = PublicActivity::Activity.where(:recipient_id => current_user.id).order('created_at desc').page(params[:page])
  end
end