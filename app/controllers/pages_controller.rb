class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  skip_before_filter :walled_garden, only: [:splash, :splash_authenticate]

  def home
    ahoy.track "homepage"
    @articles = Article.published.order('created_at DESC').page(params[:page]).per(10)
    @page_title  = "Homepage"
    @page_description = "Know Sayaahi, know Pakistan. Step in to learn the essentials you need to learn about Pakistan."
  end

  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.all
    @page_title  = "Pages index"
    authorize! :update , Page
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    if @page.blank? and can?(:create , Page)
        flash[:error] = "This page doesn't exist. Create now?"
        redirect_to new_page_path
    elsif @page.blank?
        not_found
    else
      authorize! :read , @page
      ahoy.track "readable_content" , {} , trackable: @page unless @page.user == current_user
      @page_title  = @page.title
      @page_description = teaser(@page.body)
    end
  end

  # GET /pages/new
  def new
    authorize! :create , Page
    @page_title  = "New Page"
    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
    authorize! :update , @page
    @page_title  = "Editing #{@page.title}"
  end

  # POST /pages
  # POST /pages.json
  def create
    authorize! :create , Page
    @page = Page.new(page_params)

    respond_to do |format|
      if @page.save
        format.html { redirect_to @page, notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    authorize! :update , @page
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to @page, notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    authorize! :delete , @page
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.find_by_slug(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:title, :slug, :body, :teaser)
    end
end
