class Page < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :body, :title
  before_save :manage_slug
  validates_format_of :slug , with: /\A[a-z0-9-]+\z/
  extend FriendlyId
  friendly_id :slug

  # Overriding friendly id's sluggin mechanism, using our own
  def manage_slug
    self.slug = self.title.parameterize if self.slug.blank?
  end

end
