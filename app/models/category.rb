class Category < ActiveRecord::Base
	has_many :articles
	validates_presence_of :name, :description, :icon, :intro
	validate :slug_sanity
	before_save :set_slug

	def set_slug
		self.slug = self.name.parameterize('.') if self.slug.blank?
	end

	def formatted_name
		name.capitalize
	end

	def slug_sanity
	  	unless self.slug.match(/\A[a-zA-Z0-9.]+\Z/)
	    	errors.add(:slug, "No space or symbols allowed (except period)")
	  	end
	end
end
