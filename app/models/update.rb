class Update < ActiveRecord::Base

belongs_to :user

before_save :inactivate_others

# PAPERTRAIL
has_paper_trail only: [:title, :body, :intro] , on: [:update]

# PAPERCLIP
has_attached_file :promo,
                  styles: { :large => "800x400>", :medium => "400x200>" }

# VALIDATIONS
validates_presence_of :title , :body
validates_length_of :body , in: 240..25000
validates :promo, :dimensions => { :width => 800, :height => 400 }
validates_attachment  :promo,
                      content_type: { content_type: /\Aimage\/.*\Z/ },
                      size: {in: 0..2500.kilobytes}

# PAPERCROP (ezuhaib/papercrop)
crop_attached_file :promo , min_size:'800x400' , aspect:'2:1'

private

def inactivate_others
	if self.active?
		@active_updates = Update.where(active: true).all
		@active_updates.each do |u|
			u.update_attribute(:active,false)
		end
	end
end

end
