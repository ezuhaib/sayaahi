class Article < ActiveRecord::Base
belongs_to :user
has_many :comments , as: :commentable
has_many :assignments , as: :assignable
has_many :events , class_name: "Ahoy::Event" , as: :trackable
belongs_to :category
scope :published, -> { where(workflow_state: ["published","featured"]) }
attr_accessor :remarks

# Tip:
# Keep min. width of the image 700-900px, so that the same images can be used
# for craeation of wide covers later on should we change the layout

# PAPERTRAIL
has_paper_trail only: [:title, :body, :intro] , on: [:update]

# PAPERCLIP
has_attached_file :cover,
                  styles: { :large => "800x400>", :medium => "267x200>" , :thumb => "100x75>" },
                  :default_url => "paperclip_defaults/articles/covers/:style/missing.jpg"

# VALIDATIONS
validates_presence_of :title , :body, :category_id
validates_length_of :body , in: 320..25000
validates_length_of :intro , maximum: 160
validates :cover, :dimensions => { :width => 800, :height => 400 }
validates_attachment  :cover,
                      content_type: { content_type: /\Aimage\/.*\Z/ },
                      size: {in: 0..4000.kilobytes}

# PAPERCROP (ezuhaib/papercrop)
crop_attached_file :cover , min_size:'800x400' , aspect:'2:1'

# PUBLIC ACTIVITY
include PublicActivity::Model

# WORKFLOW
include Workflow
workflow do

  state :awaiting_assignment do
    event :assign, transitions_to: :assigned , meta: {hide: true}
    event :reject, transitions_to: :cold_rejected
  end

  state :assigned do
    event :publish, transitions_to: :published
    event :reject, transitions_to: :warm_rejected
  end

  state :published
    event :unpublish , transitions_to: :unpublished, meta: {friendly: 'Yank out'}

  state :cold_rejected do
    event :unreject, transitions_to: :awaiting_assignment
  end

  state :warm_rejected do
    event :unreject, transitions_to: :assigned
  end

  state :unpublished do
    event :publish, transitions_to: :published
    event :reject, transitions_to: :warm_rejected
  end

end

# EVENT HANDLERS
def feature
  @previously_featured = Article.where(workflow_state: 'featured').first
  @previously_featured.try(:unfeature!)
end

def assigned_to?(user,roles=nil)
  assignments = self.assignments.active.where(to_id:user.id)
  if roles == nil
    return true if assignments.any?
  elsif roles.is_a? Array
    roles = roles.map { |x| x.to_s }
    return true if (assignments.pluck(:tag) & roles).any?
  end
  return false
end

def editing_assignment
  self.assignments.active.where(tag: 'editing').first
end

def subediting_assignment
  self.assignments.active.where(tag: 'subediting').first
end

def custom_assignments
  self.assignments.active.where(tag: 'custom').all
end

def latest_version
  self.versions.last.id
end

end
