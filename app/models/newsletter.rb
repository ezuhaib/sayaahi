class Newsletter < ActiveRecord::Base
validates_presence_of :title , :body , :category

def self.categories
  {
    "Global (send to everyone)" => :global,
    "Important (send to those subscibed) NOT IMPLEMENTED" => :important
  }
end

def liquid_legend(user)
  {
    "username" => user.username,
    "join_date" => user.created_at.strftime("%d %B, %Y")
  }
end

end
