class Comment < ActiveRecord::Base
belongs_to :commentable , polymorphic: true
belongs_to :user
validates_presence_of :body, :commentable_type, :commentable_id
validates_presence_of :name , if: "user_id.nil?"
validates_size_of :body , in: 40..800
validates :email , presence: true , email: true , if: "user_id.nil?"
# (later--> email: {mx: true})
after_create :initial_workflow
has_ancestry


def initial_workflow
  if self.user_id
    self.accept!
  else
    self.submit!
  end
end

# If the commentable has a defined user, they will be notified of the new comment
# If the commentable has a reciever and a sender, the non-commentator shall be notified (one or both)
# todo: Allow for notifying in more complex user lists.
# todo: optimize this code, and perhaps move it to a notifier observer/module

def accept
  if self.commentable.try(:user)
    self.commentable.create_activity :commented , owner: self.user , recipient: self.commentable.user
  elsif self.user.id == self.commentable.by_id
    self.commentable.create_activity :commented , owner: self.user , recipient: User.find(self.commentable.to_id)
  elsif self.user.id == self.commentable.to_id
    self.commentable.create_activity :commented , owner: self.user , recipient: User.find(self.commentable.by_id)
  elsif self.commentable.try(:to_id) and self.commentable.try(:by_id)
    self.commentable.create_activity :commented , owner: self.user , recipient: User.find(self.commentable.to_id)
    self.commentable.create_activity :commented , owner: self.user , recipient: User.find(self.commentable.by_id)
  end
end

# WORKFLOW
include Workflow
workflow do

  state :new do
    event :accept, :transitions_to => :accepted
    event :submit , :transitions_to => :awaiting_review
  end

  state :awaiting_review do
    event :accept, :transitions_to => :accepted
    event :reject, :transitions_to => :rejected
  end

  state :accepted do
    event :reject, :transitions_to => :rejected
  end

  state :rejected do
    event :accept , :transitions_to => :accepted
  end

end

end