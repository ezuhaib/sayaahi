class User < ActiveRecord::Base

# Include default devise modules. Others available are:
# :confirmable, :lockable, :timeoutable and :omniauthable
devise :database_authenticatable, :registerable,
       :recoverable, :rememberable, :trackable, :validatable
has_and_belongs_to_many :roles
has_many :articles
has_many :assignments , foreign_key: 'to_id'
has_many :comments
has_one :story

###############################
# LOGIN VIA USERNAME/EMAIL
###############################
attr_accessor :login

def self.find_first_by_auth_conditions(warden_conditions)
  conditions = warden_conditions.dup
  if login = conditions.delete(:login)
    where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
  else
    where(conditions).first
  end
end

################################
# FRIENDLY ID
################################
extend FriendlyId
friendly_id :username

################################
# FUNCTIONS: INSTANCE METHODS
################################
def role?(role)
  self.roles.pluck(:title).include?(role.to_s)
end

################################
# AVATAR
################################

# Add logic for male/female avatar or random avatars below
def self.set_default_avatar
  "paperclip_defaults/users/avatars/:style/avatar_default.jpg"
end

has_attached_file :avatar,
  :styles => { :thumb => "130x130#", :mini => "60x60#" , :inline => "25x25#" },
  :default_url => set_default_avatar
crop_attached_file :avatar , min_size: "300x300"

################################
# VALIDATIONS
################################

validates :username, :uniqueness => {:case_sensitive => false}
validates_presence_of :username , :gender , :dob , :body
validates_size_of :body , in: 40...2000
validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
validates_attachment_size :avatar, in: 0..2000.kilobytes
validates :avatar, :dimensions => { :width => 300, :height => 300 }
validate :username_sanity

def username_sanity
  unless self.username.match(/\A[a-zA-Z0-9.]+\Z/)
    errors.add(:username, "No space or symbols allowed (except period)")
  end
end

################################
# OPTIONS
################################
include Optionable
options email_unread_notifications:true ,
        email_site_updates:true,
        email_new_messages_count:true

end
