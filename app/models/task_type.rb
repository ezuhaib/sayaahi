class TaskType < ActiveRecord::Base
validates_presence_of :key , :title, :body, :assignable_type, :assignee_limit
validates_uniqueness_of :key , scope: :assignable_type , message: 'has already been defined for this assignable_type'

def guide_page
  if self.guide_page_slug
    Page.find_by_slug(self.guide_page_slug)
  else
    return false
  end
end

end
