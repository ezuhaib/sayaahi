class Assignment < ActiveRecord::Base
	belongs_to :assignable , polymorphic: true
	has_many :comments , as: :commentable
	validates_presence_of :by_id , :to_id, :tag, :title
	validate :assignee_limit
	validate :date_must_be_in_future
	after_save :notify_assignee
	before_validation :set_title
	scope :expired, -> { where('deadline < ?',Time.now) }
	scope :not_expired, -> { where('deadline > ?',Time.now) }
	scope :active, -> { not_expired.where(workflow_state: ['open','completed']) }

	# PUBLIC ACTIVITY
	include PublicActivity::Model
	include ActionView::Helpers::DateHelper

	# Generates titles if not provided
	def set_title
		if title.blank? && tag != "custom" && self.assignable.try(:title)
			self.title = self.assignable.title
		end
	end

	def assignee
		User.find(self.to_id)
	end

	def task_type
		TaskType.where(key: self.tag, assignable_type: self.assignable_type).first
	end

	def guide_page
		self.task_type.guide_page
	end

	def self.root_task_types
		TaskType.where(assignable_type: 'Root')
	end

	def disabled?
		self.workflow_state == "refused" || self.deadline < Time.now
	end

	# WORKFLOW
	include Workflow
	workflow do

	  state :open do
	    event :refuse, transitions_to: :refused
	    event :complete, transitions_to: :completed
	  end

	  state :refused
		state :completed

	end

	def status
		if self.workflow_state == 'refused'
			'Refused'
		elsif self.workflow_state == 'completed'
			'Completed'
		elsif deadline > Time.now
			time_ago_in_words(self.deadline) << " left"
		elsif deadline < Time.now
			'Expired'
		else
			'Unknown'
		end
	end

	private
	def notify_assignee
		self.create_activity :made , owner: User.find(self.by_id) , recipient: self.assignee
	end

	def assignee_limit
		@previous_assignments = Assignment.active.where(assignable: self.assignable, tag: self.tag).count
		@assignee_limit = TaskType.where(key: self.tag, assignable_type: self.assignable_type).first.assignee_limit || 0
		if @previous_assignments >= @assignee_limit && @assignee_limit != 0
			errors.add(:tag, "No more users allowed for this activity.")
		end
	end


  def date_must_be_in_future
    if (deadline and deadline.to_date < Date.today)
      errors.add(:deadline, "This should probably be in the future.")
    end
  end

end
