# Admin: The user with all Permissions
# Teammate: Sayaahi teammate. Access to admin menu and workroom.
#           but special priveledges granted upon assignment only
# Editor: Having editing privelege on any piece of text.
# Manager: Can make assignments

class Ability
  include CanCan::Ability

  def initialize(user)
    # https://github.com/bryanrite/cancancan/wiki/Defining-Abilities
    user ||= User.new

    # All users
    can :read , [Article, User, Update, Comment, Page]
    can [:create,:reply] , Comment

    # Authenticated Users
    if user.username
      can :read , :notification
      can :update , User do |usr|
        usr == user
      end
      can :update , Article do |article|
          article.try(:user) == user || article.assigned_to?(user , [:editing, :proofreading])
      end
      can :work , Article do |article|
          article.try(:user) == user || article.assigned_to?(user , [:editing, :proofreading])
      end
      can :update , User do |u|
          u == user
      end
      can :destroy , Article do |x|
          x.try(:user) == user
          x.created_at > 15.minutes.ago
      end
      can :read_unpublished , Article do |article|
        article.try(:user) == user || article.assigned_to?(user , [:editing, :proofreading])
      end
      can :create , Article
      can :read , Assignment do |a|
        a.try(:to_id) == user.id # Will allow others to join in assignments later
      end
    end

    # Teammates

    if user.role? :teammate
      can :access , :team_menu # but not admin_pages
    end

    # Editors
    if user.role? :editor
      can [:update, :review, :moderate] , [Article,Comment]
      can [:read_unpublished , :read_versions, :feature] , Article
      can :manage , Update
      can :access , :admin_menu
    end

    # Superadmin
    # Except for first, Permissions added for reference
    if user.role? :admin
      can :manage , :all
      can :assign , :all
      can :superedit , :all
      can :access , :admin_pages
      can :manage , [Page , Assignment, User] # For user lists with email addresses
      can :mail , User
    end

  end
end
