$(document).on('ready page:load', function() {

    $('#to_id').selectize({
   	create: false,
    maxOptions: 1,
    openOnFocus: false,
    valueField: 'id',
    labelField: 'username',
    searchField: 'username',

    load: function(query, callback) {
        if (!query.length) return callback();
        $.ajax({
            url: '/users/autocomplete?query=' + encodeURIComponent(query),
            type: 'GET',
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res);
            }
        });
    }
});

});