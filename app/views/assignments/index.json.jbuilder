json.array!(@assignments) do |assignment|
  json.extract! assignment, :id, :by_id, :to_id, :assignable_id, :assignable_type, :tag, :deadline, :body
  json.url assignment_url(assignment, format: :json)
end
