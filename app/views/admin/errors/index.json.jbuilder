json.array!(@admin_errors) do |admin_error|
  json.extract! admin_error, :id, :id, :class_name, :trace, :params, :target_url, :referer_url, :user_agent, :user_info, :app_name, :doc_root
  json.url admin_error_url(admin_error, format: :json)
end
