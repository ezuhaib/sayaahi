json.array!(@task_types) do |task_type|
  json.extract! task_type, :id, :key, :title, :body, :assignable_type, :assignee_limit
  json.url task_type_url(task_type, format: :json)
end
