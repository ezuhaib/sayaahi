json.array!(@newsletters) do |newsletter|
  json.extract! newsletter, :id, :title, :body, :category, :mailed
  json.url newsletter_url(newsletter, format: :json)
end
