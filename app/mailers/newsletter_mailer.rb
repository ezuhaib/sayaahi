class NewsletterMailer < ActionMailer::Base
  default :from => "\"Sayaahi\" <#{Figaro.env.mailer_user}>"

  def main(newsletter,user)
    @newsletter = newsletter
    @user = user
    @body = Liquid::Template.parse(@newsletter.body)
    mail(:to => @user.email, :subject => @newsletter.title)
  end
end