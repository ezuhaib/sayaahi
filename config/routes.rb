Rails.application.routes.draw do

  get 'users/autocomplete' , to: 'users#autocomplete'
  get 'articles/:id/edit/crop', to: 'articles#crop' , as: "crop_featured_image"
  get 'updates/:id/edit/crop', to: 'updates#crop' , as: "crop_promo"
  get 'users/:id/edit/crop', to: 'users#crop' , as: "crop_avatar"

  resources :updates
  resources :pages
  resources :users, constraints: {id: /[0-9A-Za-z\-\.]+/ }
  resources :notifications

  resources :assignments do
    member do
      get 'workflow'
    end
  end

  mount Ckeditor::Engine => '/editor'
  resources :newsletters do
    member do
      get :mail
      get :preview
    end
  end

  resources :comments do
    member do
      get :accept
      get :reject
      get :reply
      post :reply
    end
  end

  devise_for :user, :path => 'account', :path_names => { :sign_in => "login", :sign_out => "logout", :sign_up => "register" }

  root 'pages#home'
  #resources :updates disabled till we're ready

  resources :articles , path: 'node' , except: :index do
    member do
      get :workroom
      post :workroom , action: 'workroom_save'
      get 'version/:version', action: 'version' , as: "version"
      delete 'version/:version', action: 'destroy_version' , as: "destroy_version"
      get :assign
      post :assign , action: 'assign_save' , as: 'assign_save'
      patch :assign , action: 'assign_save'
    end
  end

  namespace :admin do
    root to: 'pages#index'
    resources :articles
    resources :comments
    resources :task_types
    resources :errors , except: [:new,:create,:update,:edit]
    resources :categories do
      member do
        get :remove
      end
    end
    get :assignments, to: 'pages#assignments'
    get :stats , to: 'stats#index'
  end

  get '/account/password' , to: 'users#password_changed'

  # The following route definition should stay at the end of this file
  get '/:category' , to: 'articles#index' , as: 'category'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase
  #   resources :products do

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
