class Ahoy::Store < Ahoy::Stores::ActiveRecordStore

  def track_event(name,properties,options)
    super do |event|
      event.trackable = options[:trackable]
    end
  end

end
